import time
import signal
import random

from splinter import Browser as WebBrowser
from utils import at_exit
from logger import Logger

USER_AGENT = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0',
    'Mozilla/5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36'
]

class Browser(Logger, object):

    def __init__(self, driver_name=None):
        super(Browser, self).__init__()

        self.browser = None
        self.driver = None
        self.driver_name = driver_name
        if not self.driver_name:
            self.driver_name = 'phantomjs'
        at_exit.register(self.quit)

    def start(self, **kwargs):
        self.browser = WebBrowser(driver_name=self.driver_name, user_agent=random.choice(USER_AGENT), service_log_path='/dev/null', **kwargs)
        self.driver = self.browser.driver
        self.driver.maximize_window()

    def scroll_down(self):
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    def scrolling_down(self, times):
        for i in range(1, times):
            self.scroll_down()
            time.sleep(0.5)

    def slow_scroll(self, interval=None, wait=1):
        if interval is None:
            window = self.driver.get_window_size()
            interval = window['height']*0.8

        height = int(self.driver.execute_script("return document.body.scrollHeight;"))
        scroll = interval
        while height > interval and interval:
            self.driver.execute_script("window.scrollTo(0, {});".format(scroll))
            time.sleep(wait)
            scroll += interval
            height -= interval

    def quit(self):
        try:
            self.browser.driver.service.process.send_signal(signal.SIGTERM)
            self.browser.driver.quit()
        except:
            pass
