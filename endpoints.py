__author__ = 'Khoir MS'

import re

class Endpoints:

    def __init__(self):
        self.url_path = list()
        self.base_url = 'https://www.facebook.com/search/'

        self.__people_pronoun = {
            'f': 'me/friends/', # Friends
            'nf': 'me/non-friends/', # Non-friends
            'fof': 'me/friends/friends/' # Friends of friends
        }
        # only for people pronoun friends of friends selected
        self.__not_my_friends = 'me/non-friends/'

        # self.__genders = ['males/', 'females/']
        # self.__interested_in = [
        #     'males/users-interested/',
        #     'females/users-interested/'
        # ]

        self.__relationship = {
            'single': 'single/users/', # Single
            'inarlxn': 'in-any-relationship/users/', # In a Relationship
            'inopenrlxn': 'in-open-relationship/users/', # In an Open Relationship
            'married': 'married/users/', # Married
            'civilunion': 'in-civil-union/users/', # In a Civil Union
            'domesticpartnership': 'in-domestic-partnership/users/', # In a Domestic Partnership
            'engaged': 'engaged/users/', # Engaged
            'itscomplicated': 'its-complicated/users/', # It's Complicated
            'widowed': 'widowed/users/', # Widowed
            'separated': 'separated/users/', # Separated
            'divorced': 'divorced/users/', # Divorced
            'dating': 'dating/users/', # Dating
        }
        # people visited
        self.__locations = {
            'current': '{}/residents/present/', # Current location
            'former': '{}/residents/past/', # Former location
            'visited': '{}/visitors/', # Visited
            'from': '{}/home-residents/', # From
            'lives_near': '{}/residents-near/present/', # Lives near
        }
        self.__interests = '{}/likers/'
        self.__company = {
            'current': '{}/employees/present/',
            'former': '{}/employees/past/'
        }
        self.__school = {
            'current': '{}/students/present/',
            'former': '{}/students/past/'
        }
        self.__jobtitle = '{}/employees/'
        self.__language = '{}/speakers/'
        self.__major = '{}/major/students/present/'

        self.__born_year_before = '{}/before/users-born/'
        self.__born_year_after = '{}/after/users-born/'
        self.__born_month = [
            'jan', 'feb', 'mar', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
        ]
        self.__people_name = 'str/{}/users-named/'

    def __set_search_type(self, s_type, fof=False):
        if not (s_type or s_type in self.__people_pronoun): return
        result = self.__people_pronoun[s_type]
        self.url_path.append(result)

        if s_type == 'fof' and fof:
            self.url_path.append(self.__not_my_friends)

    def __set_gender(self, gender):
        if not gender in ('males', 'females'): return
        result = '{}/'.format(gender)

        self.url_path.append(result)

    def __set_interested_in(self, gender):
        if not gender in ('males', 'females'): return
        result = '{}/users-interested/'.format(gender)

        self.url_path.append(result)

    def __set_relationship(self, relationship):
        if not relationship in self.__relationship.keys(): return
        result = self.__relationship[relationship]

        self.url_path.append(result)

    def __set_location(self, location, currentpast=None):
        if not location: return

        if not (currentpast or currentpast in self.__locations.keys()):
            currentpast = 'current'

        if not re.search(r'\d+', str(location)):
            location = 'str/{}/pages-named'.format(location)
        result = self.__locations[currentpast].format(location)

        self.url_path.append(result)

    def __set_interest(self, interest):
        if not interest: return
        interest = 'str/{}/pages-named'.format(interest)
        result = self.__interests.format(interest)

        self.url_path.append(result)

    def __set_company(self, company, currentpast=None):
        if not company: return

        if not (currentpast or currentpast in self.__company.keys()):
            currentpast = 'current'

        if not re.search(r'\d+', str(company)):
            company = 'str/{}/pages-named'.format(company)
        result = self.__company[currentpast].format(company)

        self.url_path.append(result)

    def __set_school(self, school, currentpast=None):
        if not school: return

        if not (currentpast or currentpast in self.__school.keys()):
            currentpast = 'current'

        if not re.search(r'\d+', str(school)):
            school = 'str/{}/pages-named'.format(school)
        result = self.__school[currentpast].format(school)

        self.url_path.append(result)

    def __set_jobtitle(self, title):
        if not title: return
        if not re.search(r'\d+', str(title)):
            title = 'str/{}/pages-named'.format(title)
        result = self.__jobtitle.format(title)

        self.url_path.append(result)

    def __set_language(self, lang):
        if not lang: return
        if not re.search(r'\d+', str(lang)):
            lang = 'str/{}/pages-named'.format(lang)
        result = self.__language.format(lang)

        self.url_path.append(result)

    def __set_major(self, major):
        if not major: return
        if not re.search(r'\d+', str(major)):
            major = 'str/{}/pages-named'.format(major)
        result = self.__major.format(major)

        self.url_path.append(result)

    def __set_born(self, year, month=''):
        if not year: return

        if isinstance(year, list) and len(year) == 2:
            bya = self.__born_year_after.format(year[0])
            self.url_path.append(bya)

            byb = self.__born_year_before.format(year[1])
            self.url_path.append(byb)
        else:
            if not month in self.__born_month: month = ''
            if not month:
                born_month = '{}/date/users-born/'.format(year)
            else:
                born_month = '{}/{}/date-2/users-born/'.format(year, month)
            self.url_path.append(born_month)

    def __set_people_name(self, name):
        if not name: return
        result = self.__people_name.format(name)

        self.url_path.append(result)

    def peoples(self, s_type='', fof=False, gender='', interested_id='', relationship='', location='', location_currentpast=None,
                interest='', company='', company_currentpast=None, school='', school_currentpast=None, jobtitle='', lang='',
                major='', born_year=None, born_month='', people_name=''):
        self.__set_search_type(s_type=s_type, fof=fof)
        self.__set_gender(gender=gender)
        self.__set_interested_in(gender=interested_id)
        self.__set_relationship(relationship=relationship)
        self.__set_location(location=location, currentpast=location_currentpast)
        self.__set_interest(interest=interest)
        self.__set_company(company=company, currentpast=company_currentpast)
        self.__set_school(school=school, currentpast=school_currentpast)
        self.__set_jobtitle(title=jobtitle)
        self.__set_language(lang=lang)
        self.__set_major(major=major)
        self.__set_born(year=born_year, month=born_month)
        self.__set_people_name(name=people_name)

        if not self.url_path: return
        url_path = ''.join(self.url_path)
        search_query = '{}{}intersect/'.format(self.base_url, url_path)
        # reset url path
        self.url_path = list()

        return search_query

    def friends_of(self, fb_id):
        search_query = '{}{}/friends?ref=pivot'.format(self.base_url, fb_id)

        return search_query

    def events(self):
        pass

    def photos(self):
        pass

    def posts(self):
        pass

if __name__=='__main__':
    print '=' * 30, 'BASIC', '=' * 30
    print Endpoints().peoples(s_type='f')
    print Endpoints().peoples(s_type='nf')
    print Endpoints().peoples(s_type='fof')
    print Endpoints().peoples(s_type='fof', fof=True)

    print '=' * 30, 'GENDER', '=' * 30
    print Endpoints().peoples(s_type='f', gender='males')
    print Endpoints().peoples(s_type='f', gender='females')

    print '=' * 30, 'INSTERESTED IN', '=' * 30
    print Endpoints().peoples(s_type='f', interested_id='males')
    print Endpoints().peoples(s_type='f', interested_id='females')

    print '=' * 30, 'RELATIONSHIP', '=' * 30
    print Endpoints().peoples(s_type='f', relationship='single')
    print Endpoints().peoples(s_type='f', relationship='inarlxn')
    print Endpoints().peoples(s_type='f', relationship='inopenrlxn')
    print Endpoints().peoples(s_type='f', relationship='married')
    print Endpoints().peoples(s_type='f', relationship='civilunion')
    print Endpoints().peoples(s_type='f', relationship='domesticpartnership')
    print Endpoints().peoples(s_type='f', relationship='engaged')
    print Endpoints().peoples(s_type='f', relationship='itscomplicated')
    print Endpoints().peoples(s_type='f', relationship='widowed')
    print Endpoints().peoples(s_type='f', relationship='separated')
    print Endpoints().peoples(s_type='f', relationship='divorced')
    print Endpoints().peoples(s_type='f', relationship='dating')

    print '=' * 30, 'LOCATIONS', '=' * 30
    print Endpoints().peoples(s_type='f', location='bandung')
    print Endpoints().peoples(s_type='f', location='bandung', location_currentpast='former')
    print Endpoints().peoples(s_type='f', location='bandung', location_currentpast='visited')
    print Endpoints().peoples(s_type='f', location='bandung', location_currentpast='from')
    print Endpoints().peoples(s_type='f', location='bandung', location_currentpast='lives_near')
    print Endpoints().peoples(s_type='f', location='103847742987074')
    print Endpoints().peoples(s_type='f', location='103847742987074', location_currentpast='former')
    print Endpoints().peoples(s_type='f', location='103847742987074', location_currentpast='visited')
    print Endpoints().peoples(s_type='f', location='103847742987074', location_currentpast='from')
    print Endpoints().peoples(s_type='f', location='103847742987074', location_currentpast='lives_near')

    print '=' * 30, 'COMPANY', '=' * 30
    print Endpoints().peoples(s_type='f', company='ebdesk')
    print Endpoints().peoples(s_type='f', company='150452628420886')
    print Endpoints().peoples(s_type='f', company='ebdesk', company_currentpast='former')

    print '=' * 30, 'SCHOOL', '=' * 30
    print Endpoints().peoples(s_type='f', school='smk 4 padalarang')
    print Endpoints().peoples(s_type='f', school='smk 4 padalarang', school_currentpast='former')

    print '=' * 30, 'JOB', '=' * 30
    print Endpoints().peoples(s_type='f', jobtitle='programmer')

    print '=' * 30, 'LANGUAGE', '=' * 30
    print Endpoints().peoples(s_type='f', lang='sundaneese')

    print '=' * 30, 'MAJOR', '=' * 30
    print Endpoints().peoples(s_type='f', major='english')

    print '=' * 30, 'BORN', '=' * 30
    print Endpoints().peoples(s_type='f', born_year='1994')
    print Endpoints().peoples(s_type='nf', born_year='1993', born_month='apr')
    print Endpoints().peoples(s_type='f', born_year=['1993', '1995'])

    print '=' * 30, 'BY NAME', '=' * 30
    print Endpoints().peoples(s_type='f', people_name='ahmad')
    print Endpoints().peoples(s_type='nf', people_name='wkwkwk', location='lampung', location_currentpast='from')
    print Endpoints().peoples()
    print