# Facebook Search Wrapper

Library untuk pencarian orang difacebook menggunakan kriteria seperti pada page https://searchisback.com/

# Menggunakan

- [x] Python2.7
- [x] PhantomJS atau Chrome Headless
- [x] Selenium
- [x] PyQuery
- [x] BeautifulSoup