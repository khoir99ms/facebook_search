__author__ = 'Khoir MS'

import re
import json

from logger import Logger
from auth import BASE_URL
from utils.html import html_entities
from pyquery import PyQuery as PyQ
from bs4 import BeautifulSoup as BS4

class Extractor(object):

    def __init__(self):
        self.logger = Logger(self.__class__.__name__)

        self.pyq = None
        self.document = None

    def set_document(self, doc, parser=None):
        self.pyq = PyQ(doc, parser=parser)
        self.pyq.make_links_absolute(BASE_URL)
        self.document = self.pyq('html').outer_html()

    def get_field_value(self, record, parent=None, parser=None):
        exit = None
        if not parent is None:
            if record['type'] == 'attr':
                if record['selector']:
                    exit = PyQ(parent, parser=parser)(record['selector']).attr(record['attr'])
                else:
                    exit = PyQ(parent, parser=parser).attr(record['attr'])
            elif record['type'] == 'text':
                if record['selector']:
                    exit = PyQ(parent, parser=parser)(record['selector']).text()
                else:
                    exit = PyQ(parent, parser=parser).text()
            elif record['type'] == 'html':
                if record['selector']:
                    exit = PyQ(parent, parser=parser)(record['selector']).html()
                else:
                    exit = PyQ(parent, parser=parser).html()
            elif record['type'] == 'style':
                exit = record['attr']
        else:
            if record['type'] == 'attr':
                exit = self.pyq(record['selector']).attr(record['attr'])
            elif record['type'] == 'text':
                exit = self.pyq(record['selector']).text()
            elif record['type'] == 'html':
                exit = self.pyq(record['selector']).html()
            elif record['type'] == 'style':
                exit = record['attr']
        if record['type'] in ['text', 'html']:
            exit = html_entities(exit)

        return exit

class Search(Extractor):

    __TEMPLATE = {'id': '', 'url': '', 'name': '', 'username': '', 'photo': ''}

    def __init__(self):
        super(Search, self).__init__()
        self.last_scroll_count = 1

    def peoples(self, html=None):
        if html:
            self.set_document(doc=html)

        selectors = {
            'id': {'selector': 'div > div[data-bt]', 'type': 'attr', 'attr': 'data-bt'},
            'url': {'selector': 'div.clearfix > div > div > div > div > a[href]', 'type': 'attr', 'attr': 'href'},
            'name': {'selector': 'div.clearfix > div > div > div > div > a[href] > div', 'type': 'text'},
            'photo': {'selector': 'div > div[data-bt] > div > div.clearfix > a > img', 'type': 'attr', 'attr': 'src'},
            'intro': {'selector': 'div.clearfix + div', 'type': 'html'}
        }

        elements = self.pyq('#BrowseResultsContainer > div, div[id$=browse_result_below_fold] > div > div, div[id^=fbBrowseScrollingPagerContainer] > div:not(#browse_end_of_results_footer) > div')
        self.logger.log("has {} peoples".format(len(elements)))
        for element in elements[self.last_scroll_count - 1:]:
            self.last_scroll_count += 1
            result = self.__TEMPLATE.copy()

            user_id = self.get_field_value(record=selectors['id'], parent=element)
            if user_id:
                user_id = json.loads(user_id)
                user_id = user_id.get('id')
            result['id'] = user_id
            result['url'] = self.get_field_value(record=selectors['url'], parent=element)
            if not (result['id'] or result['url']):
                continue

            result['name'] = self.get_field_value(record=selectors['name'], parent=element)
            username = re.sub(r'.*facebook\.com\/([^?]+).*', '\g<1>', result['url'], re.U)
            if username != 'profile.php':
                result['username'] = username
            result['photo'] = self.get_field_value(record=selectors['photo'], parent=element)
            result['intro_list'] = self.__get_intro_list(element)

            raw_intro = self.get_field_value(record=selectors['intro'], parent=element)
            intro = self.__generate_intro(raw_intro)
            result.update(intro)

            yield result

    def __generate_intro(self, raw):
        raw = re.sub(r'<(\w+)[^>]*>', '<\g<1>>', raw, re.MULTILINE | re.DOTALL)
        raw = re.sub(r'[ ]+', ' ', raw)

        result = {
            'current_city': self.__get_current_city(raw),
            'home_town': self.__get_home_town(raw),
            'work': self.__get_work(raw),
            'education': self.__get_education(raw),
            'relationship': self.__get_relationship(raw),
            'gender': self.__get_gender(raw),
            'birthday': self.__get_birthday(raw),
            'interested_in': self.__get_interested_in(raw)
        }

        return result

    def __get_intro_list(self, raw):
        intro = PyQ(raw)
        header = intro('div.clearfix + div > div:eq(0)')
        footer = intro('div.clearfix + div > div:eq(1) > div > div')
        result = list(header) + list(footer)
        for i, item in enumerate(result):
            content = PyQ(item).text()
            content = html_entities(content)

            result[i] = content.strip()
        return result

    def __get_current_city(self, raw):
        ms = re.search(r'\bLive[sd] in <\w+>([^<]+)', raw, re.I)
        if bool(ms):
            ms = ms.group(1)
        return ms

    def __get_home_town(self, raw):
        ms = re.search(r'\bFrom <\w+>([^<]+)', raw, re.I)
        if bool(ms):
            ms = ms.group(1)
        return ms

    def __get_education(self, raw):
        edu = None
        ms = re.search(r'\b(Studie[sd](.+?)?at|Went to)[^<]+<\w+>([^<]+)', raw, re.I)
        if bool(ms):
            edu = list(ms.groups())
            edu = map(lambda x: x.strip() if x else None, edu)
            edu = filter(lambda v: v, edu)
            if len(edu) > 1:
                edu = ' at '.join(edu[1:])
                edu = edu[0].upper() + edu[1:]
        return edu

    def __get_work(self, raw):
        work = None
        ms = re.search(r'\b(Work[eds]+(.+?)at)[^<]+<\w+>([^<]+)', raw, re.I)
        if bool(ms):
            work = list(ms.groups())
            work = map(lambda x: x.strip() if x else None, work)
            work = filter(lambda v: v, work)
            if len(work) > 1:
                work = ' at '.join(work[1:])
                work = work[0].upper() + work[1:]
        return work

    def __get_relationship(self, raw):
        ms = re.search(r'\b(Single|In a relationship|Engaged|Married)', raw, re.I)
        if bool(ms):
            ms = ms.group(1)
        return ms

    def __get_gender(self, raw):
        ms = re.search(r'\b([Fe]+male)', raw, re.I)
        if bool(ms):
            ms = ms.group(1)
        return ms

    def __get_birthday(self, raw):
        ms = re.search(r'\b(\d+ years old)', raw, re.I)
        if bool(ms):
            ms = ms.group(1)
        return ms

    def __get_interested_in(self, raw):
        ms = re.search(r'\bInterested in (\w+)', raw, re.I)
        if bool(ms):
            ms = ms.group(1)
        return ms

if __name__ == '__main__':
    s = Search()
    r = list()
    f = open('template.html').read()
    for i in s.peoples(f):
        r.append(i)
        print i
    print json.dumps(r, indent=4)