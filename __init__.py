import time

from datetime import datetime
from utils.general import json_build
from auth import Auth
from endpoints import Endpoints
from extractor import Search as SearchExtractor, html_entities

class Facebook(Auth):

    wait = 3

    def __init__(self, cookies=None, username=None, password=None, driver_name=None, wait=None, **kwargs):
        super(Facebook, self).__init__(driver_name=driver_name)
        self.__endpoints = Endpoints()

        if cookies or (username and password):
            self.prepare(cookies, username, password, **kwargs)
        if isinstance(wait, int):
            self.wait = wait
        self.is_empty_result = False
        self.is_not_understand_result = False

    def prepare(self, cookies, username=None, password=None, cookies_only=True, **kwargs):
        self.start(**kwargs)
        self.login(cookies, username, password, cookies_only=cookies_only)
        self.set_language()

    def search_peoples(self, s_type='', fof=False, gender='', interested_id='', relationship='', location='', location_currentpast=None,
                interest='', company='', company_currentpast=None, school='', school_currentpast=None, jobtitle='', lang='',
                major='', born_year=None, born_month='', people_name=''):
        ids = set()
        n = scroll = found = 0
        self.search_extractor = SearchExtractor()

        try:
            search_url = self.__endpoints.peoples(s_type=s_type, fof=fof, gender=gender, interested_id=interested_id,
                                                  relationship=relationship, location=location, location_currentpast=location_currentpast,
                                                  interest=interest, company=company, company_currentpast=company_currentpast,
                                                  school=school, school_currentpast=school_currentpast, jobtitle=jobtitle, lang=lang,
                                                  major=major, born_year=born_year, born_month=born_month, people_name=people_name)
            self.log('fetching query search url :', search_url)
            self.browser.visit(search_url)
            time.sleep(1)

            self.is_empty_result = self.browser.is_element_present_by_id('empty_result_error')
            self.__set_timeout()

            alive = True
            while alive:
                try:
                    layer = self.browser.find_by_css('.uiLayer:not(.uiContextualLayerPositioner)')
                    if layer:
                        layer.first.click()
                except:
                    pass

                self.scroll_down()
                time.sleep(1)

                self.is_not_understand_result = bool(self.browser.find_by_xpath('//div[contains(text(), "Please try saying this another way")]'))
                end_of_result = bool(self.browser.find_by_id('browse_end_of_results_footer'))
                alive = not (self.is_empty_result or self.is_not_understand_result or end_of_result)

                html = self.browser.find_by_id('contentArea').first
                if not html:
                    alive = False
                else:
                    for data in self.search_extractor.peoples(html=html['outerHTML']):
                        if not data['id'] in ids:
                            found += 1
                            data = json_build(data)
                            try:
                                yield data
                            except GeneratorExit:
                                return
                            ids.add(data['id'])
                        scroll = 0
                    time.sleep(1)

                n += 1
                scroll += 1
                self.log('scroll {} times'.format(n))

                if alive and scroll > 9:
                    alive = False

            self.log('found {} people'.format(found))
            if self.is_empty_result:
                self.log('is empty result')
            if self.is_not_understand_result:
                self.log('is not understand result')
        except:
            self.log(level='error')
            raise

    def friends_of(self, fb_id):
        ids = set()
        n = scroll = found = 0
        self.search_extractor = SearchExtractor()

        try:
            search_url = self.__endpoints.friends_of(fb_id=fb_id)
            self.log('fetching query search url :', search_url)
            self.browser.visit(search_url)
            time.sleep(1)

            self.is_empty_result = self.browser.is_element_present_by_id('empty_result_error')
            self.__set_timeout()

            alive = True
            while alive:
                try:
                    layer = self.browser.find_by_css('.uiLayer:not(.uiContextualLayerPositioner)')
                    if layer:
                        layer.first.click()
                        time.sleep(0.5)
                except:
                    pass

                self.scroll_down()
                time.sleep(1)

                self.is_not_understand_result = bool(self.browser.find_by_xpath('//div[contains(text(), "Please try saying this another way")]'))
                end_of_result = bool(self.browser.find_by_id('browse_end_of_results_footer'))
                alive = not (self.is_empty_result or self.is_not_understand_result or end_of_result)

                html = self.browser.find_by_id('contentArea').first
                if not html:
                    alive = False
                else:
                    for data in self.search_extractor.peoples(html=html['outerHTML']):
                        if not data['id'] in ids:
                            found += 1
                            data = json_build(data)
                            try:
                                yield data
                            except GeneratorExit:
                                return
                            ids.add(data['id'])
                        scroll = 0
                    time.sleep(1)

                n += 1
                scroll += 1
                self.log('scroll {} times'.format(n))
                if alive and scroll > 9:
                    alive = False

            self.log('found {} people'.format(found))
            if self.is_empty_result:
                self.log('is empty result')
            if self.is_not_understand_result:
                self.log('is not understand result')
        except:
            self.log(level='error')
            raise

    def __set_timeout(self):
        i = 1
        start = datetime.now()
        # returns True if the element is not present and False if is present.
        while self.browser.is_element_not_present_by_id('BrowseResultsContainer') and not self.is_empty_result:
            end = (datetime.now() - start).seconds
            if end > 120:
                raise Exception('request timeout')
            if i % 10 == 0:
                self.log('reload page at {}'.format(end))
                self.browser.reload()
            self.log('try to waiting people list for {} times'.format(i))
            i += 1

if __name__=='__main__':
    import json
    import pickle

    cookies = open('cookies/r332.txt').read()
    cookies = pickle.loads(cookies)
    # fb = Facebook(driver_name='chrome', executable_path='/home/khoir/Workspace/chromedriver')
    fb = Facebook(driver_name='phantomjs')
    fb.prepare(cookies=cookies)
    for s in fb.search_peoples(location='klaten', people_name='dwiki'):
    # for s in fb.search_peoples(location='klaten', people_name='lilik'):
        print s

    # fb.prepare(cookies=cookies)
    # for s in fb.friends_of(fb_id=100002305321392):
    # for s in fb.friends_of(fb_id=100007977575585):
    # for s in fb.friends_of(fb_id=100007542695294):
    # for s in fb.friends_of(fb_id=100011222945519):
    #     print json.dumps(s, indent=4)
        # print fb.is_not_understand_result