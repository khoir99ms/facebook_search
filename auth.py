import json
import pickle
import urlparse

from selenium.common.exceptions import WebDriverException, NoSuchElementException
from browser import Browser

BASE_URL = 'https://www.facebook.com/'
BASIC_BASE_URL = 'https://mbasic.facebook.com/'
MOBILE_BASE_URL = 'https://m.facebook.com/'
SIGN_LOGIN = [
    'https://mbasic.facebook.com/home.php',
    'https://mbasic.facebook.com/gettingstarted',
    'https://mbasic.facebook.com/login/save-device'
]
COOKIES_FORMAT = "document.cookie = '{name}={value}; path={path}; domain={domain}; expiry={expiry}; expires={expires}';"

class Auth(Browser):

    cookies = None
    login_status = False

    def __init__(self, driver_name=None):
        super(Auth, self).__init__(driver_name=driver_name)

    def login(self, cookies=None, username=None, password=None, cookies_only=True):
        self.login_status = False
        self.browser.visit(BASIC_BASE_URL)
        if cookies:
            self.log('login using cookies')
            self.set_cookies(cookies)
        elif username and password:
            self.log('login using username and password')
            self.log('username : %s' % username)
            self.log('password : %s' % password)
            self.browser.fill('email', username)
            self.browser.fill('pass', password)
            self.browser.find_by_name('login').first.click()
        else:
            self.log('login failed', level='error')

        if not self.is_login():
            if cookies and username and password and not cookies_only:
                self.log('log in with cookies failed, trying to login with username and password')
                self.login(username=username, password=password)
            else:
                raise Exception('failed initiate robot')

        self.login_status = True
        self.log('robot is ready to use')

    def is_login(self):
        self.browser.visit(urlparse.urljoin(BASIC_BASE_URL, 'home.php'))
        for sl in SIGN_LOGIN:
            if sl in self.browser.url:
                return True
        return False

    def set_cookies(self, cookies=None):
        try:
            if cookies:
                self.cookies = cookies

            if isinstance(self.cookies, str) or isinstance(self.cookies, unicode):
                try:
                    self.cookies = pickle.loads(self.cookies)
                except Exception, e:
                    self.log(e)
                    self.cookies = json.loads(self.cookies)

            self.verify_cookies()
            self.browser.driver.delete_all_cookies()
            if isinstance(self.cookies, list):
                for c in self.cookies:
                    cookie = COOKIES_FORMAT.format(**c)
                    self.browser.driver.execute_script(cookie)
                return True
            elif isinstance(self.cookies, dict):
                cookie = COOKIES_FORMAT.format(**self.cookies)
                self.browser.driver.execute_script(cookie)
                return True
            else:
                return False
        except:
            self.log(level='error')
            return False

    def verify_cookies(self):
        if not self.cookies:
            raise Exception('cookies not defined')

        if isinstance(self.cookies, dict):
            self.cookies = [self.cookies]
        cookies = list()
        for cookie in self.cookies:
            if 'name' not in cookie:
                cookie['name'] = None
            if 'value' not in cookie:
                cookie['value'] = None
            if 'path' not in cookie:
                cookie['path'] = None
            if 'domain' not in cookie:
                cookie['domain'] = None
            if 'expiry' not in cookie:
                cookie['expiry'] = None
            if 'expires' not in cookie:
                cookie['expires'] = None
            cookies.append(cookie)
        self.cookies = cookies

    def set_language(self, lang='en_US'):
        try:
            if urlparse.urljoin(BASIC_BASE_URL, 'home.php') not in self.browser.url:
                self.browser.visit(BASIC_BASE_URL)
            flag = self.browser.find_by_xpath("//a[contains(@href, '#header')]").first
            if not flag.value.lower().startswith('back to top'):
                self.browser.visit('{}/language.php'.format(BASIC_BASE_URL))
                self.browser.find_by_xpath("//a[contains(@href, '{}')]".format(lang)).first.click()
            return True
        except (WebDriverException, NoSuchElementException) as e:
            self.log(e)
            return False
